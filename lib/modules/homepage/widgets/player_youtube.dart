import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class PlayerYoutubeVideo extends StatefulWidget {
  final video;
  final title;
  final channelTitle;
  const PlayerYoutubeVideo(
      {Key? key,
      required this.video,
      required this.title,
      required this.channelTitle})
      : super(key: key);

  @override
  _YoutubeAppDemoState createState() => _YoutubeAppDemoState();
}

class _YoutubeAppDemoState extends State<PlayerYoutubeVideo> {
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();

    _controller = YoutubePlayerController(
      initialVideoId: widget.video.toString(),
      params: const YoutubePlayerParams(
        showControls: true,
        showFullscreenButton: true,
        desktopMode: false,
        privacyEnhanced: true,
        useHybridComposition: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return Scaffold(
      backgroundColor: const Color(0xFF262626),
      body: SingleChildScrollView(
          child: MediaQuery.of(context).size.width >= 300
              ? Column(
                  children: [
                    SizedBox(
                      height: 500,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: YoutubePlayerControllerProvider(
                            // Passing controller to widgets below.
                            controller: _controller,
                            child: Stack(
                              children: [
                                player,
                                Positioned.fill(
                                  child: YoutubeValueBuilder(
                                    controller: _controller,
                                    builder: (context, value) {
                                      return AnimatedCrossFade(
                                        firstChild: SizedBox.fromSize(),
                                        secondChild: Material(
                                          child: DecoratedBox(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                  YoutubePlayerController
                                                      .getThumbnail(
                                                    videoId: _controller
                                                        .initialVideoId,
                                                    quality:
                                                        ThumbnailQuality.medium,
                                                  ),
                                                ),
                                                fit: BoxFit.fitWidth,
                                              ),
                                            ),
                                            child: const Center(
                                              child: CircularProgressIndicator(),
                                            ),
                                          ),
                                        ),
                                        crossFadeState: value.isReady
                                            ? CrossFadeState.showFirst
                                            : CrossFadeState.showSecond,
                                        duration:
                                            const Duration(milliseconds: 300),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: ListTile(
                            title: Text(
                              widget.title,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            subtitle: Text(
                              widget.channelTitle,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                )
              : Column(
                  children: [
                    Expanded(
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: YoutubePlayerControllerProvider(
                            // Passing controller to widgets below.
                            controller: _controller,
                            child: Stack(
                              children: [
                                player,
                                Positioned.fill(
                                  child: YoutubeValueBuilder(
                                    controller: _controller,
                                    builder: (context, value) {
                                      return AnimatedCrossFade(
                                        firstChild: SizedBox.fromSize(),
                                        secondChild: Material(
                                          child: DecoratedBox(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                  YoutubePlayerController
                                                      .getThumbnail(
                                                    videoId: _controller
                                                        .initialVideoId,
                                                    quality:
                                                        ThumbnailQuality.medium,
                                                  ),
                                                ),
                                                fit: BoxFit.fitWidth,
                                              ),
                                            ),
                                            child: const Center(
                                              child:
                                                  CircularProgressIndicator(),
                                            ),
                                          ),
                                        ),
                                        crossFadeState: value.isReady
                                            ? CrossFadeState.showFirst
                                            : CrossFadeState.showSecond,
                                        duration:
                                            const Duration(milliseconds: 300),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: Text(
                                widget.title,
                                style: const TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              subtitle: Text(
                                widget.channelTitle,
                                style: const TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
    );
  }
}
