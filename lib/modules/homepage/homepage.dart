// ignore_for_file: unrelated_type_equality_checks

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:job_frontend_developer/models/video_youtube.dart';
import 'package:job_frontend_developer/repository/customdio/custom_dio_youtube.dart';

import 'package:job_frontend_developer/repository/customdio/youtube_repository/repository_youtube.dart';
import 'package:job_frontend_developer/repository/customdio/youtube_repository/state.dart';

import 'widgets/player_youtube.dart';

class HomepageWidget extends StatefulWidget {
  const HomepageWidget({Key? key}) : super(key: key);

  @override
  _HomepageWidgetState createState() => _HomepageWidgetState();
}

class _HomepageWidgetState extends State<HomepageWidget> {
  CustomDioYoutube dio = CustomDioYoutube();
  var bloc = VideosYoutube(dio: CustomDioYoutube());

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) =>
            VideosYoutube(dio: dio)..buscarVideos(bloc.searchTextController),
        child: BlocBuilder<VideosYoutube, StateYoutube>(
          builder: (context, state) => Scaffold(
            key: scaffoldKey,
            backgroundColor: const Color(0xFF262626),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image:
                              Image.asset('lib/shared/image/bandatocando.jpg')
                                  .image,
                        ),
                        boxShadow: const [
                          BoxShadow(
                            blurRadius: 4,
                            color: Colors.transparent,
                            offset: Offset(0, 2),
                          )
                        ],
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                          topLeft: Radius.circular(0),
                          topRight: Radius.circular(0),
                        ),
                      ),
                      child: Container(
                        width: 100,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Color(0xFF1E2429), Color(0x00111417)],
                            stops: [0, 1],
                            begin: AlignmentDirectional(0, -1),
                            end: AlignmentDirectional(0, 1),
                          ),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(16),
                            bottomRight: Radius.circular(16),
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsetsDirectional.fromSTEB(
                              16, 56, 16, 0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                padding:
                                    EdgeInsetsDirectional.fromSTEB(0, 4, 0, 0),
                                child: Center(
                                  child: Text(
                                    'Descubra suas bandas ',
                                    style: TextStyle(
                                      fontFamily: 'Lexend Deca',
                                      color: Colors.white,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsetsDirectional.fromSTEB(
                                    0, 16, 0, 12),
                                child: Center(
                                  child: Container(
                                    width: MediaQuery.of(context).size.width >=
                                            720
                                        ? MediaQuery.of(context).size.width *
                                            0.60
                                        : MediaQuery.of(context).size.width *
                                            0.80,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: const Color(0x9AFFFFFF),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsetsDirectional
                                                      .fromSTEB(16, 0, 0, 0),
                                              child: TextFormField(
                                                onEditingComplete: () {
                                                  context
                                                      .read<VideosYoutube>()
                                                      .buscarVideos(bloc
                                                          .searchTextController);
                                                },
                                                controller:
                                                    bloc.searchTextController,
                                                obscureText: false,
                                                decoration:
                                                    const InputDecoration(
                                                  hintText:
                                                      'Pesquisar por nome',
                                                  hintStyle: TextStyle(
                                                    fontFamily: 'Lexend Deca',
                                                    color: Color(0xFF1A1F24),
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: Color(0x00000000),
                                                      width: 1,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(4.0),
                                                      topRight:
                                                          Radius.circular(4.0),
                                                    ),
                                                  ),
                                                  focusedBorder:
                                                      UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: Color(0x00000000),
                                                      width: 1,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(4.0),
                                                      topRight:
                                                          Radius.circular(4.0),
                                                    ),
                                                  ),
                                                ),
                                                style: const TextStyle(
                                                  fontFamily: 'Lexend Deca',
                                                  color: Color(0xFF1A1F24),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Card(
                                            clipBehavior:
                                                Clip.antiAliasWithSaveLayer,
                                            color: const Color(0xFF1E2429),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                            ),
                                            child: IconButton(
                                                splashColor: Colors.transparent,
                                                splashRadius: 30,
                                                iconSize: 46,
                                                icon: const Icon(
                                                  Icons.search_outlined,
                                                  color: Colors.white,
                                                  size: 24,
                                                ),
                                                onPressed: () {
                                                  context
                                                      .read<VideosYoutube>()
                                                      .buscarVideos(bloc
                                                          .searchTextController);
                                                }),
                                          )
                                        ]),
                                  ),
                                ),
                              ),
                              const ListaVideos()
                            ],
                          ),
                        ),
                      ),
                    ),

                    /* Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: SizedBox(
                          height: MediaQuery.of(context).size.height * 0.50,
                          child: const PlayerYoutubeVideo()),
                    ),
                    const DetalhesBanda(),
                    // const TileVideo(), */
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

class ListaVideos extends StatefulWidget {
  const ListaVideos({Key? key}) : super(key: key);

  @override
  _ListaVideosState createState() => _ListaVideosState();
}

class _ListaVideosState extends State<ListaVideos> {
  late TextEditingController idController;
  @override
  void initState() {
    super.initState();
    idController = TextEditingController();
  }

  videoId(video, channelTitle, title) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => PlayerYoutubeVideo(
                  video: video,
                  title: title,
                  channelTitle: channelTitle,
                )));
  }

  @override
  Widget build(BuildContext context) {
    final status = context.select((VideosYoutube cubit) => cubit.state.status);
    switch (status) {
      case StatusYoutube.initial:
        return SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: const Center(
            child: Text(
              "Pesquise acima para obter as informações de suas bandas.",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        );
      case StatusYoutube.loading:
        return const Center(
          child: CircularProgressIndicator(),
        );
      case StatusYoutube.failure:
        return const Center(
          child: Text(
            "Requisição Falhou!",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        );
      case StatusYoutube.success:
        var listaDados = context
            .select((VideosYoutube cubit) => cubit.state.videosstate) as List;
       return (MediaQuery.of(context).size.width >= 480)
            ? GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 400,
                    childAspectRatio: 3 / 4,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                padding: const EdgeInsets.only(
                    right: 10, left: 10, bottom: 20, top: 20),
                itemCount: listaDados.length,
                scrollDirection: Axis.vertical,
                physics: const ScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  // List<VideoModel>? videos = snapshot.data;
                  VideoModel video = listaDados[index];

                  // return const SizedBox(
                  //   child: Text('aaaaaaaa'),
                  //   width: 200,
                  // );

                  return InkWell(
                    onTap: () =>
                        videoId(video.id, video.channelTitle, video.title),
                    child: Column(
                      children: [
                        Container(
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(color: Colors.grey),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    video.imagem,
                                  ))),
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              video.title,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            subtitle: Text(
                              video.channelTitle,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              )
            : GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 400,
                    childAspectRatio: 1,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                padding: const EdgeInsets.only(
                    right: 10, left: 10, bottom: 20, top: 20),
                itemCount: listaDados.length,
                scrollDirection: Axis.vertical,
                physics: const ScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  // List<VideoModel>? videos = snapshot.data;
                  VideoModel video = listaDados[index];

                  // return const SizedBox(
                  //   child: Text('aaaaaaaa'),
                  //   width: 200,
                  // );

                  return InkWell(
                    onTap: () =>
                        videoId(video.id, video.channelTitle, video.title),
                    child: Column(
                      children: [
                        Container(
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(color: Colors.grey),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    video.imagem,
                                  ))),
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              video.title,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            subtitle: Text(
                              video.channelTitle,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
    }
  }
}
