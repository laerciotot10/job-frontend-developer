import 'package:flutter/material.dart';

extension ContextExtensions on BuildContext {
  
  bool get isDesktop => MediaQuery.of(this).size.width >= 720;
  
}
