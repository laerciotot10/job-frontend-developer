import 'package:equatable/equatable.dart';

class VideoModel extends Equatable {
final  String id;
 final dynamic idChannel;
 final String title;
 final String? url;
 final String? thumbnailUrl;
 final String channelTitle;
 final String? publishedAt;
 final String imagem;

  const VideoModel({this.idChannel, required this.channelTitle, this.publishedAt, 
       required this.id, required this.title, this.url, this.thumbnailUrl, required this.imagem});

/*   VideoModel.fromJson(Map<String, dynamic> json) {
    id = json['id']['videoId'] ?? "";
    idChannel = json['snippet']['channelId'] ?? "";
    publishedAt = json['snippet']['publishedAt'] ?? "";
    channelTitle = json['snippet']['channelTitle'] ?? "";
    title = json['snippet']['title'] ?? "";
    thumbnailUrl = json["snippet"]["thumbnails"]["url"] ?? "";
    imagem = json["snippet"]["thumbnails"]["medium"]["url"] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id ?? "";
    data['title'] = title ?? "";
    data['url'] = url ?? "";
    data['thumbnailUrl'] = thumbnailUrl ?? "";
    data['idChannel'] = idChannel ?? "";
    data['publishedAt'] = publishedAt ?? "";
    data['channelTitle'] = channelTitle ?? "";
    data['imagem'] = imagem ?? "";
    return data;
  } */

  @override
  List<Object?> get props => [
        id,
        title,
        url,
        thumbnailUrl,
        idChannel,
        channelTitle,
        publishedAt,
        imagem
      ];
}
