import 'package:dio/dio.dart';

class CustomDioYoutube {
  final Dio client = Dio();

  CustomDioYoutube() {
    client.options.headers['content-Type'] = 'application/json; charset=utf-8';
    client.options.baseUrl = "https://www.googleapis.com/youtube/v3";
    client.options.connectTimeout = 30000;
  }
}
