import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:job_frontend_developer/models/video_youtube.dart';
import 'package:job_frontend_developer/repository/constantes_acesso.dart';
import 'package:job_frontend_developer/repository/customdio/custom_dio_vagalume.dart';
import 'package:job_frontend_developer/repository/customdio/custom_dio_youtube.dart';
import 'package:job_frontend_developer/repository/customdio/youtube_repository/state.dart';

class VideosYoutube extends Cubit<StateYoutube> {
  CustomDioYoutube dio = CustomDioYoutube();
  CustomDioVagalume diovagalume = CustomDioVagalume();
  CustomDioVagalumeArtista dioVagalumeArtista = CustomDioVagalumeArtista();

  List<VideoModel> list = [];
 // List<DadosbandaModel>? urlbanda;

  final TextEditingController searchTextController = TextEditingController();

  VideosYoutube({required this.dio}) : super(const StateYoutube());

  Future<List<VideoModel>> buscarVideos(
      TextEditingController searchTextController) async {
    try {
      emit(StateYoutube(
          videosstate: state.videosstate, status: StatusYoutube.loading));
      await Future.delayed(const Duration(seconds: 2));

      print("tentei");
      var response = await dio.client.get('/search', queryParameters: {
        'key': apikeyyoutube,
        'part': 'snippet,id',
        'order': 'date',
        'maxResults': 15,
        'q': searchTextController.text,
      });

      print(searchTextController.text);

      if (response.statusCode == 200) {
        print("FOI");
        print(response.data['items']);

        if (response.data["items"] is List) {
          /*  return videosstate = (response.data["items"] as List)
              .map((videoMap) => VideoModel.fromJson(videoMap))
              .toList();
             */
          final body = (response.data["items"]) as List;
          final videosstate = body.map((dynamic json) {
            return VideoModel(
              channelTitle: json['snippet']['channelTitle'] ?? "",
              title: json['snippet']['title'] ?? "",
              imagem: json["snippet"]["thumbnails"]["medium"]["url"] ?? "",
              id: json['id']['videoId'] ?? "",
            );
          }).toList();
          emit(StateYoutube(
              videosstate: videosstate, status: StatusYoutube.success));
        }
      }

      return [];
    } on DioError catch (e) {
      emit(StateYoutube(
          videosstate: state.videosstate, status: StatusYoutube.failure));
      (e);
      throw (e.message);
    }
  }

  /* Future<List<DadosbandaModel>> getdadosbanda() async {
    try {
      print("tentei busca vagalume");
      var response = await diovagalume.client.get("/search.art?",
          queryParameters: {
            'apikey': apikeyvagalume,
            'limit': 5,
            'q': searchTextController.text
          });

      if (response.statusCode == 200) {
        print(response.data);

        if (response.data[0] is List) {
          /*  return videosstate = (response.data["items"] as List)
              .map((videoMap) => VideoModel.fromJson(videoMap))
              .toList();
             */
          final body = (response.data[0]) as List;
          final dadosbanda = body.map((dynamic json) {
            return DadosbandaModel(
              url: json["docs"] ?? "",
            );
          }).toList();
          urlbanda = dadosbanda;
          print(" dados"+ dadosbanda.toString());
        }
      }
      return [];
    } on DioError catch (e) {
      (e);
      throw (e.message);
    }
  }

  Future<void> getdadosartista() async {
    await Future.delayed(const Duration(seconds: 3));
    try {
      print("tentei artista vagalume");
      print("aqui vamos la" + urlbanda.toString());
      var response = await dioVagalumeArtista.client.get("/u2/index.js");

      if (response.statusCode == 200) {
        print(response.data);

        if (response.data["artist"] is List) {
          /*  return videosstate = (response.data["items"] as List)
              .map((videoMap) => VideoModel.fromJson(videoMap))
              .toList();
             */
          final body = (response.data["artist"]) as List;
          final dadosartista = body.map((dynamic json) {
            return ArtistaModel(
              descricao: json["desc"] ?? "",
              genero: json["genre"]["name"] ?? "",
              posicao: json["rank"]["pos"] ?? "",
              toplyrics: json["toplyrics"]["item"]["desc"] ?? "",
              url: json["toplyrics"]["item"]["url"] ?? "",
            );
          }).toList();
        }
      }
    } on DioError catch (e) {
      (e);
      throw (e.message);
    }
  } */
}
