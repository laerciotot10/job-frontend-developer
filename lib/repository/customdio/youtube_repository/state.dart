
import 'package:equatable/equatable.dart';

import 'package:job_frontend_developer/models/video_youtube.dart';

enum StatusYoutube {initial,loading,success,failure}

class StateYoutube extends Equatable {
  const StateYoutube({
    this.status = StatusYoutube.initial,
    this.videosstate,
  });

  final StatusYoutube status;
  final List<VideoModel>? videosstate; 

  @override
  List<Object?> get props => [status,videosstate];
}
