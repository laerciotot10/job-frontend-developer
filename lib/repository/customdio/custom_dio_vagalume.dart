import 'package:dio/dio.dart';

class CustomDioVagalume {
  final Dio client = Dio();

  CustomDioVagalume() {
    client.options.headers['content-Type'] = 'application/json; charset=utf-8';
    client.options.baseUrl = "https://api.vagalume.com.br/";
    client.options.connectTimeout = 30000;
  }
}

class CustomDioVagalumeArtista {
  final Dio client = Dio();

  CustomDioVagalumeArtista() {
    client.options.headers['content-Type'] = 'application/json; charset=utf-8';
    client.options.baseUrl = "https://www.vagalume.com.br";
    client.options.connectTimeout = 30000;
  }
}